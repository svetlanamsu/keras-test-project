import numpy as np
import sys
import mlflow

from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Dropout

from time import time


def gen_data(input_dim=20, bsize=1000):

    # Generate dummy data for training and test set
    x_train = np.random.random((bsize, input_dim))
    y_train = np.random.randint(2, size=(bsize, 1))
    x_test = np.random.random((int(bsize * 0.10), input_dim))
    y_test = np.random.randint(2, size=(int(bsize * 0.10), 1))

    return [x_train, y_train, x_test, y_test]

def build_model(in_dim=20, drate=0.5, out=64):
    mdl = Sequential()
    mdl.add(Dense(out, input_dim=in_dim, activation='relu'))
    if drate:
        mdl.add(Dropout(drate))
    mdl.add(Dense(out, activation='relu'))
    if drate:
        mdl.add(Dropout(drate))
    mdl.add(Dense(1, activation='sigmoid'))

    return mdl

def compile_and_run_model(mdl, train_data, epochs=20, batch_size=128):
    mdl.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

    mdl.fit(train_data[0], train_data[1],
          epochs=epochs,
          batch_size=bs,
          verbose=0)

    score = mdl.evaluate(train_data[2], train_data[3], batch_size=batch_size)
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])

    print("Predictions for Y:")
    print(mdl.predict(train_data[2][:5]))
    mdl.summary()

    return (score[0], score[1], mdl)

if __name__ == '__main__':
    print('Run Keras project')

    drop_rate = float(sys.argv[1]) 
    input_dim = int(sys.argv[2]) 
    bs = int(sys.argv[3]) 
    output = int(sys.argv[4]) 
    train_batch_size = int(sys.argv[5]) 
    epochs = int(sys.argv[6])

    print("drop_rate", drop_rate)
    print("input_dim", input_dim)
    print("size", bs)
    print("output", output)
    print("train_batch_size", train_batch_size)
    print("epochs", epochs)

    data = gen_data(input_dim=input_dim, bsize=train_batch_size)
    model = build_model(in_dim=input_dim, drate=drop_rate, out=output)

    with mlflow.start_run():
        mlflow.keras.autolog()
        start_time = time()
        loss, acc, mdl = compile_and_run_model(model, data, epochs=epochs, batch_size=bs)
        timed = time() - start_time
        print("This model took", timed, "seconds to train and test.")
        mlflow.log_metric("Time to run", timed)
        mlflow.keras.log_model(mdl, "model")
